# bash files
cd ~/
rm .bashrc
ln -s ~/dotfiles/.bashrc
ln -s ~/dotfiles/.bash_aliases

# alacritty
cd ~/.config
mkdir alacritty
cd alacritty
ln -s ~/dotfiles/alacritty.toml

# wezterm
cd ~/.config
mkdir wezterm
cd wezterm
ln -s ~/dotfiles/wezterm.lua

# neovim
cd ~/.config 
mkdir nvim
cd nvim
ln -s ~/dotfiles/init.lua

# latexmk
cd ~/
ln -s ~/dotfiles/.latexmkrc

# zathura
cd ~/.config/
mkdir zathura/
cd zathura/
ln -s ~/dotfiles/zathurarc
