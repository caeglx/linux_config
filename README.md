## Idea of storring the most used config files in another directory for a single git repository
Files that are getting stored:
- .bashrc: bash config including some aliases
    - location: ~/.bashrc
- alacritty.yml: alacrtty config, mainly for font style and color theme
    - location: ~/.config/alacritty/alacritty.yml
- init.lua: neovim config. For Lazy plugins, their config and keymaps.
    - location: ~/.config/nvim/init.lua
- wezterm.lua: currently not in use
