-- Pull in the wezterm API
local wezterm = require("wezterm")

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
	config = wezterm.config_builder()
end

-- This is where you actually apply your config choices

-- For example, changing the color scheme:
-- config.color_scheme = "GitHub Dark"
config.color_scheme = "Catppuccin Frappe"
-- config.color_scheme = "Gruvbox Material (Gogh)"
-- config.font = wezterm.font("JetBrainsMono Nerd Font")
config.font = wezterm.font("FiraCode Nerd Font Mono")
config.cell_width = 1.0
config.font_size = 12
-- No title bar, which is not needed since there is the tab bar at the top
-- config.window_decorations = "NONE"
-- and finally, return the configuration to wezterm
return config
