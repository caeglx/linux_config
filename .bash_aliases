# ---- Aliases for classic commands ----

# use nala when typing apt; apt is still the backend
apt() {
  command nala "$@"
}
sudo() {
  if [ "$1" = "apt" ]; then
    shift
    command sudo nala "$@"
  else
    command sudo "$@"
  fi
}

cat() {
  command batcat "$@"
}

# use ll for ls -lah
alias ll='ls -lah'
alias l='ls -CF'

# easier disk usage
alias dus='du -sh'
alias dud='du -sh ./*'

# fuzzy finder for dirs
fcd() {
    local dir
    dir=$(find ~ -type d 2> /dev/null | fzf)
    if [[ -n "$dir" ]]; then
        cd "$dir"
    fi
}

# ---- Aliases for own commands ----

# use radio station as radio
radio(){
	command ./venv/bin/radio --search "$@"
}
# --------- Obsidian workspace ---------
alias obs='cd ~/Documents/00_workspace && nvim'
alias obsw='cd ~/Documents/00_workspace/Arbeit && nvim'

# --------- Battery info ---------------
alias batteryinfo='upower -i `upower -e | grep 'BAT'`'

# --------- LaTeX ----------------------

# --------- Python environments ---------
# pyhon roche mixing
alias mix='cd ~/Documents/11_python/01_roche_mixing && source ./venv_roche_mixing/bin/activate && nvim'

# python analysis
alias ana='cd ~/Documents/11_python/ && source ./00_learning/venv_learning/bin/activate && ./start_jupyter'

# get to python start venv and open neovim
alias pyjl='cd ~/Documents/11_python/00_learning/ && source ./venv_learning/bin/activate && jupyter-lab'
alias pynl='cd ~/Documents/11_python/00_learning/ && source ./venv_learning/bin/activate && nvim'

alias pyjj='cd ~/Documents/11_python/03_jufo/ && source ./venv_jufo/bin/activate && jupyter-lab'
alias pynj='cd ~/Documents/11_python/03_jufo/ && source ./venv_jufo/bin/activate && nvim'

alias juju='cd ~/Documents/15_julia/ && source ./venv_julia/bin/activate && jupyter-lab'

# --- Java development
jr(){
  filename=$(basename -- "$1")
  classname="${filename%.*}"
  javac "$1"

  if [ $? -eq 0 ]; then
    java "$classname"
  else
    	echo "Compilation failed."
  fi
}

# --- System programs
alias gpumeter='watch -d -n 0.5 nvidia-smi'

alias updatesys='sudo apt update && sudo apt upgrade && flatpak update'

# -------- Phone backup using imobiledevice ----------
alias backupphone='idevicebackup2 backup ~/Documents/09_phone/'
alias restorephone='idevicebackup2 --source 00008120-001A64DA36A1A01E --system --settings --reboot ~/Documents/09_phone/'

# -------- Server administration ----------
alias luksconnect="ssh -i ~/.ssh/thinkcentre -i 'HostKeyAlgorithms ssh-rsa ' -p 2222 root@192.168.178.30"
